﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace FakeNews.Model
{
    public class NewsItems
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string DateLine { get; set; }
        public string Subhead { get; set; }
        public string Image { get; set; }

    }
    public class NewsManager
    {
        public static void GetNews(string category, ObservableCollection<NewsItems> newsItems)
        {
            var allItems = getNewsItems();
            var filteredNewsItems = allItems.Where(p => p.Category == category).ToList();
            newsItems.Clear();
            filteredNewsItems.ForEach(p => newsItems.Add(p));
        }
        private static List<NewsItems> getNewsItems()
        {
            var items = new List<NewsItems>();
            items.Add(new NewsItems() { Id = 1, Category = "Financial", Headline = "Lorem Ipsum", DateLine = "Nunc Tristique nec", Subhead = "Doro sit amet", Image = "Assets/images/.png " });
            items.Add(new NewsItems() { Id = 2, Category = "Financial", Headline = "Etian ac felis viverra", DateLine = "tortor porttitor,eu fermentum ante congue", Subhead = "vulputata nisl ac , aliquet nisi", Image = "Assets/images/Financia.png " });
            items.Add(new NewsItems() { Id = 3, Category = "Financial", Headline = "Integer sed turpis erat", DateLine = "in viverra metus facilisis sed", Subhead = "Sed quis hendrerit lorem , quis interdum dolor", Image = "Assets/images/Financial3.png " });
            items.Add(new NewsItems() { Id = 4, Category = "Financial", Headline = "Proin sem neque", DateLine = "Integer eleifend", Subhead = "aliquet quis ipsum tincidunt", Image = "Assets/Financial4.png " });
            items.Add(new NewsItems() { Id = 5, Category = "Financial", Headline = "Mauris bibendum non leo vitae tempor", DateLine = "Curabitur dictum augue vitae element ultrices", Subhead = "In nisl tortor , eleifend sed ipsum eget", Image = "Assets/images/Financial5.png " });
            items.Add(new NewsItems() { Id = 6, Category = "Food", Headline = "Lorem ispum", DateLine = "Nunc tristique", Subhead = "Dolor sit amet", Image = "Assets/images/Food1.png " });
            items.Add(new NewsItems() { Id = 7, Category = "Food", Headline = "Etiam ac felis viverra", DateLine = "Nunc tristique", Subhead = "Dolor sit amet", Image = "Assets/images/Food2.png " });
            items.Add(new NewsItems() { Id = 8, Category = "Food", Headline = "Integer sed turpis erat", DateLine = "Nunc tristique", Subhead = "Dolor sit amet", Image = "Assets/images/Food3.png " });
            items.Add(new NewsItems() { Id = 9, Category = "Food", Headline = "proin sem neque", DateLine = "Nunc tristique", Subhead = "Dolor sit amet", Image = "Assets/images/Food4.png " });
            items.Add(new NewsItems() { Id = 10, Category = "Food", Headline = "Mauris bibendum non leo vitae tempor", DateLine = "", Subhead = "Dolor sit amet", Image = "Assets/images/Food5.png " });
            return items;
        }
    }
}
