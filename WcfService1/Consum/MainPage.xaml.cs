﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Consum;
using Consum.ServiceReference1;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Consum
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
      EmployeeNewClient webService = new EmployeeNewClient();
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            getEmployee();
        }

        async void getEmployee()
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;
                GridViewEmployee.ItemsSource = await webService.GetProductListAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
            catch (Exception ex)
            {
                MessageDialog messageDialog = new MessageDialog(ex.Message);
                await messageDialog.ShowAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }

        private async void GridViewEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems.Count != 0)
                {
                    Table_1 selectedEmployee = e.AddedItems[0] as Table_1;
                    TextBoxName.Text = selectedEmployee.FirstName_;
                    TextBoxAge.Text = selectedEmployee.Age_.ToString();
                    TextBoxCity.Text = selectedEmployee.Address_;
                    TextBoxSurname.Text = selectedEmployee.LastName_;
                }
            }
            catch
            {
                MessageDialog message = new MessageDialog("Error Data");
                await message.ShowAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }

        private async void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;
                Table_1 newEmployee = new Table_1();
                newEmployee.FirstName_ = TextBoxName.Text;
                newEmployee.LastName_ = TextBoxSurname.Text;
                newEmployee.Address_ = TextBoxCity.Text;
                newEmployee.Age_ = Int32.Parse(TextBoxAge.Text);

                bool result = await webService.AddEmployee2Async(newEmployee);
                ProgressBar.IsIndeterminate = false;
                ProgressBar.Visibility = Visibility.Collapsed;
                if (result == true)
                {
                    MessageDialog message = new MessageDialog("Inserted Successfully");
                    await message.ShowAsync();
                    Reset();
                }
                else
                {
                    MessageDialog message = new MessageDialog("Cannot Insert");
                    await message.ShowAsync();
                }
                getEmployee();
            }
            catch (Exception ex)
            {
                MessageDialog messageDialog = new MessageDialog(ex.Message);
                await messageDialog.ShowAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }

        private void Reset()
        {
            TextBoxName.Text = string.Empty;
            TextBoxAge.Text = string.Empty;
            TextBoxCity.Text = string.Empty;
            TextBoxSurname.Text = string.Empty;
        }

        private async void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (GridViewEmployee.SelectedItem != null)
            {
                try
                {
                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;
                    bool result = await webService.DeleteEmployee2Async((GridViewEmployee.SelectedItem as Table_1).empID);
                    if (result == true)
                    {
                        MessageDialog message = new MessageDialog("Deleted Successfully");
                        await message.ShowAsync();
                        Reset();
                    }
                    else
                    {
                        MessageDialog message = new MessageDialog("Cannot Delete");
                        await message.ShowAsync();
                    }
                    getEmployee();
                }
                catch (Exception ex)
                {

                    MessageDialog messageDialog = new MessageDialog(ex.Message);
                    await messageDialog.ShowAsync();
                    ProgressBar.Visibility = Visibility.Collapsed;
                    ProgressBar.IsIndeterminate = false;
                }
            }

            else
            {
                MessageDialog message = new MessageDialog("Choose Record to Delete");
                await message.ShowAsync();
            }
        }

        private async void ButtonModify_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;

                Table_1 newEmployee = new Table_1();
                newEmployee.empID = (GridViewEmployee.SelectedItem as Table_1).empID;
                newEmployee.FirstName_ = TextBoxName.Text;
                newEmployee.LastName_ = TextBoxSurname.Text;
                newEmployee.Address_ = TextBoxCity.Text;
                newEmployee.Age_ = Int32.Parse(TextBoxAge.Text);

                bool result = await webService.UpdateEmployeeAsync(newEmployee);
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;

                if (result == true)
                {
                    MessageDialog message = new MessageDialog("Edited Successfully");
                    await message.ShowAsync();
                    Reset();
                }
                else
                {
                    MessageDialog message = new MessageDialog("Cannot Edit");
                    await message.ShowAsync();
                }
            }
            catch
            {
                MessageDialog message = new MessageDialog("Choose Record to Edit");
                await message.ShowAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }
    }
}
