﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeNew" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeNew.svc or EmployeeNew.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployeeNew
    {
        DataClasses1DataContext data = new DataClasses1DataContext();
        public bool AddEmployee2(Table_1 eml)
        {
            try
            {
                data.Table_1s.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
            throw new NotImplementedException();
        }

        public bool DelteEmployee2(int idE)
        {
            try
            {
                Table_1 employeeDelete =
                    (from table1 in data.Table_1s where table1.empID == idE select table1).Single();
                data.Table_1s.DeleteOnSubmit(employeeDelete);
                data.SubmitChanges();
                return true;

            }
            catch
            {
                return false;
            }

        }

    

        public List<Table_1> GetProductList()
        {
            try
            {
                return (from table1 in data.Table_1s select table1).ToList();
            }
            catch
            {
                return null;
            }
        }
         
        public bool UpdateEmployee (Table_1 eml)
        {
            Table_1 employeeToModify=
                (from table1 in data.Table_1s where table1.empID == eml.empID select table1).Single();
            employeeToModify.FirstName_ = eml.FirstName_;
            employeeToModify.LastName_ = eml.LastName_;
            employeeToModify.Age_ = eml.Age_;
            employeeToModify.Address_ = eml.Address_;
            return true;
        }
    }
}
